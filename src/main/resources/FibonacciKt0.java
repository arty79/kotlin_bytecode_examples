package test.java;

import kotlin.Metadata;
import kotlin.ResultKt;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.coroutines.jvm.internal.Boxing;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlin.sequences.Sequence;
import kotlin.sequences.SequenceScope;
import kotlin.sequences.SequencesKt;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(
   mv = {1, 6, 0},
   k = 2,
   d1 = {"\u0000 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\b\u0002\u001a\u0019\u0010\u0005\u001a\u00020\u00062\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b¢\u0006\u0002\u0010\n\"\u0017\u0010\u0000\u001a\b\u0012\u0004\u0012\u00020\u00020\u0001¢\u0006\b\n\u0000\u001a\u0004\b\u0003\u0010\u0004¨\u0006\u000b"},
   d2 = {"fibonacci", "Lkotlin/sequences/Sequence;", "", "getFibonacci", "()Lkotlin/sequences/Sequence;", "main", "", "args", "", "", "([Ljava/lang/String;)V", "speech"}
)
public final class FibonacciKt0 {
   @NotNull
   private static final Sequence fibonacci = SequencesKt.sequence((Function2)(new Function2((Continuation)null) {
      // $FF: synthetic field
      private Object sequenceScope;
      int cur;
      int next;
      int label;

      @Nullable
      public final Object invokeSuspend(@NotNull Object $result) {
         Object var6 = IntrinsicsKt.getCOROUTINE_SUSPENDED();
         Integer result;
         SequenceScope sequenceScope;
         int cur;
         int next;
         int tmp;
         switch(this.label) {
         case 0:
            ResultKt.throwOnFailure($result);
            sequenceScope = (SequenceScope)this.sequenceScope;
            cur = 1;
            next = 1;
            result = Boxing.boxInt(1);
            this.sequenceScope = sequenceScope;
            this.cur = cur;
            this.next = next;
            this.label = 1;
            if (sequenceScope.yield(result, this) == var6) {
               return var6;
            }
            break;
         case 1:
            next = this.next;
            cur = this.cur;
            sequenceScope = (SequenceScope)this.sequenceScope;
            ResultKt.throwOnFailure($result);
            break;
         case 2:
            next = this.next;
            cur = this.cur;
            sequenceScope = (SequenceScope)this.sequenceScope;
            ResultKt.throwOnFailure($result);
            tmp = cur + next;
            cur = next;
            next = tmp;
            break;
         default:
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
         }

         while(true) {
            result = Boxing.boxInt(next);
            this.sequenceScope = sequenceScope;
            this.cur = cur;
            this.next = next;
            this.label = 2;
            if (sequenceScope.yield(result, this) == var6) {
               return var6;
            }

            tmp = cur + next;
            cur = next;
            next = tmp;
         }
      }

      @NotNull
      public final Continuation create(@Nullable Object value, @NotNull Continuation completion) {
         Intrinsics.checkNotNullParameter(completion, "completion");
         Function2 var3 = new <anonymous constructor>(completion);
         var3.L$0 = value;
         return var3;
      }

      public final Object invoke(Object var1, Object var2) {
         return ((<undefinedtype>)this.create(var1, (Continuation)var2)).invokeSuspend(Unit.INSTANCE);
      }
   }));

   @NotNull
   public static final Sequence getFibonacci() {
      return fibonacci;
   }

   public static final void main(@NotNull String[] args) {
      Intrinsics.checkNotNullParameter(args, "args");
      String var1 = SequencesKt.joinToString$default(SequencesKt.take(fibonacci, 10), (CharSequence)null, (CharSequence)null, (CharSequence)null, 0, (CharSequence)null, (Function1)null, 63, (Object)null);
      System.out.println(var1);
   }
}
