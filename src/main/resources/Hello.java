import kotlinx.coroutines.DelayKt;
import kotlin.jvm.functions.Function1;
import kotlin.collections.CollectionsKt;
import java.util.ArrayList;
import kotlinx.coroutines.CoroutineStart;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.BuildersKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.ResultKt;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.Unit;
import kotlinx.coroutines.CoroutineScope;
import kotlin.jvm.functions.Function2;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import org.jetbrains.annotations.Nullable;
import kotlin.coroutines.Continuation;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import kotlin.coroutines.jvm.internal.DebugMetadata;
import kotlin.Metadata;
import kotlin.coroutines.jvm.internal.ContinuationImpl;

@Metadata(mv = { 1, 4, 2 }, bv = { 1, 0, 3 }, k = 3, d1 = { "\u0000\u001a\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010!\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\u0010\u0000\u001a\u0004\u0018\u00010\u00012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006H\u0086@" }, d2 = { "expensiveComputation", "", "res", "", "", "continuation", "Lkotlin/coroutines/Continuation;", "" })
@DebugMetadata(f = "Main.kt", l = { 19 }, i = { 0 }, s = { "L$0" }, n = { "res" }, m = "expensiveComputation", c = "MainKt")
static final class Hello extends ContinuationImpl {
   /* synthetic */ Object result;
   int label;
   Object L$0;

   @Nullable
   public final Object invokeSuspend(@NotNull final Object $result) {
      this.result = $result;
      this.label |= Integer.MIN_VALUE;
      return MainKt.expensiveComputation((List)null, (Continuation)this);
   }

   Hello(final Continuation continuation) {
      super(continuation);
   }
}@DebugMetadata(f = "Main.kt", l = { 11 }, i = {}, s = {}, n = {}, m = "invokeSuspend", c = "LambdaF")
@Metadata(mv = { 1, 4, 2 }, bv = { 1, 0, 3 }, k = 3, d1 = { "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u008a@¢\u0006\u0004\b\u0003\u0010\u0004" }, d2 = { "<anonymous>", "", "Lkotlinx/coroutines/CoroutineScope;", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;" })
static final class LambdaF extends SuspendLambda implements Function2<CoroutineScope, Continuation<? super Unit>, Object> {
   int label;
   final /* synthetic */ CoroutineContext this;

   @Nullable
   public final Object invokeSuspend(@NotNull final Object o2) {
      final Object coroutine_SUSPENDED = IntrinsicsKt.getCOROUTINE_SUSPENDED();
      switch (this.label) {
         case 0: {
            ResultKt.throwOnFailure(o);
            final List $res = this.this.$res;
            this.label = 1;
            if (MainKt.expensiveComputation($res, (Continuation)this) == coroutine_SUSPENDED) {
               return coroutine_SUSPENDED;
            }
            break;
         }
         case 1: {
            ResultKt.throwOnFailure(o2);
            break;
         }
         default: {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
         }
      }
      return Unit.INSTANCE;
   }

   LambdaF(final CoroutineContext this, final Continuation continuation) {
      this.this = this;
      super(2, continuation);
   }

   @NotNull
   public final Continuation<Unit> create(@Nullable final Object value, @NotNull final Continuation<?> completion) {
      Intrinsics.checkNotNullParameter((Object)completion, "completion");
      return (Continuation<Unit>)new LambdaF(this.this, (Continuation)completion);
   }

   public final Object invoke(final Object o, final Object o2) {
      return ((LambdaF)this.create(o, (Continuation)o2)).invokeSuspend((Object)Unit.INSTANCE);
   }
}@DebugMetadata(f = "Main.kt", l = {}, i = {}, s = {}, n = {}, m = "invokeSuspend", c = "CoroutineContext")
@Metadata(mv = { 1, 4, 2 }, bv = { 1, 0, 3 }, k = 3, d1 = { "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u008a@¢\u0006\u0004\b\u0003\u0010\u0004" }, d2 = { "<anonymous>", "", "Lkotlinx/coroutines/CoroutineScope;", "invoke", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;" })
static final class CoroutineContext extends SuspendLambda implements Function2<CoroutineScope, Continuation<? super Unit>, Object> {
   private /* synthetic */ Object L$0;
   int label;
   final /* synthetic */ List $res;

   @Nullable
   public final Object invokeSuspend(@NotNull final Object o) {
      IntrinsicsKt.getCOROUTINE_SUSPENDED();
      switch (this.label) {
         case 0: {
            ResultKt.throwOnFailure(o);
            final CoroutineScope $this$runBlocking = (CoroutineScope)this.L$0;
            BuildersKt.launch$default($this$runBlocking, (CoroutineContext)null, (CoroutineStart)null, (Function2)new LambdaF(this, (Continuation)null), 3, (Object)null);
            this.$res.add("Hello,");
            return Unit.INSTANCE;
         }
         default: {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
         }
      }
   }

   CoroutineContext(final List $res, final Continuation continuation) {
      this.$res = $res;
      super(2, continuation);
   }

   @NotNull
   public final Continuation<Unit> create(@Nullable final Object value, @NotNull final Continuation<?> completion) {
      Intrinsics.checkNotNullParameter((Object)completion, "completion");
      final CoroutineContext CoroutineContext = new CoroutineContext(this.$res, (Continuation)completion);
      CoroutineContext.L$0 = value;
      return (Continuation<Unit>)CoroutineContext;
   }

   public final Object invoke(final Object o, final Object o2) {
      return ((CoroutineContext)this.create(o, (Continuation)o2)).invokeSuspend((Object)Unit.INSTANCE);
   }
}@Metadata(mv = { 1, 4, 2 }, bv = { 1, 0, 3 }, k = 2, d1 = { "\u0000\u001a\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0011\n\u0000\u001a\u001f\u0010\u0000\u001a\u00020\u00012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u0086@\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0005\u001a\u0019\u0010\u0006\u001a\u00020\u00012\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00040\b¢\u0006\u0002\u0010\t\u0082\u0002\u0004\n\u0002\b\u0019" }, d2 = { "expensiveComputation", "", "res", "", "", "(Ljava/util/List;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "main", "args", "", "([Ljava/lang/String;)V" })
public final class MainKt {
   public static final void main(@NotNull final String[] args) {
      Intrinsics.checkNotNullParameter((Object)args, "args");
      final List res = (List)new ArrayList();
      BuildersKt.runBlocking$default((CoroutineContext)null, (Function2)new MainKt$main.CoroutineContext(res, (Continuation)null), 1, (Object)null);
      System.out.println((Object)CollectionsKt.joinToString$default((Iterable)res, (CharSequence)" ", (CharSequence)null, (CharSequence)null, 0, (CharSequence)null, (Function1)null, 62, (Object)null));
   }

   @Nullable
   public static final Object expensiveComputation(@NotNull List<String> l$0, @NotNull final Continuation<? super Unit> continuation) {
      final Continuation $continuation;
      Label_0045: {
         if (continuation instanceof MainKt$expensiveComputation.Hello) {
            final MainKt$expensiveComputation.Hello Hello = (MainKt$expensiveComputation.Hello)continuation;
            if ((Hello.label & Integer.MIN_VALUE) != 0x0) {
               final MainKt$expensiveComputation.Hello mainKt$expensiveComputation$2 = Hello;
               mainKt$expensiveComputation$2.label -= Integer.MIN_VALUE;
               break Label_0045;
            }
         }
         $continuation = (Continuation)new MainKt$expensiveComputation.Hello((Continuation)continuation);
      }
      final Object $result = ((MainKt$expensiveComputation.Hello)$continuation).result;
      final Object coroutine_SUSPENDED = IntrinsicsKt.getCOROUTINE_SUSPENDED();
      switch (((MainKt$expensiveComputation.Hello)$continuation).label) {
         case 0: {
            ResultKt.throwOnFailure($result);
            final long n = 1000L;
            final Continuation continuation2 = $continuation;
            ((MainKt$expensiveComputation.Hello)$continuation).L$0 = l$0;
            ((MainKt$expensiveComputation.Hello)$continuation).label = 1;
            if (DelayKt.delay(n, continuation2) == coroutine_SUSPENDED) {
               return coroutine_SUSPENDED;
            }
            break;
         }
         case 1: {
            l$0 = (List)((MainKt$expensiveComputation.Hello)$continuation).L$0;
            ResultKt.throwOnFailure($result);
            break;
         }
         default: {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
         }
      }
      l$0.add("world!");
      return Unit.INSTANCE;
   }
}