package test;

import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import test.NonCapLambdaKt.num.1;

@Metadata(
   mv = {1, 6, 0},
   k = 2,
   xi = 48,
   d1 = {"\u0000\u001e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0004\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\n\u001a\u0006\u0010\u000b\u001a\u00020\u0003\u001a7\u0010\f\u001a\u00020\u0003\"\u0004\b\u0000\u0010\r2\u001c\u0010\u000e\u001a\u0018\u0012\u0004\u0012\u0002H\r\u0012\u0004\u0012\u00020\u00030\u0001j\b\u0012\u0004\u0012\u0002H\r`\u00042\u0006\u0010\u000f\u001a\u0002H\r¢\u0006\u0002\u0010\u0010\"'\u0010\u0000\u001a\u0018\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001j\b\u0012\u0004\u0012\u00020\u0002`\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0014\u0010\u0007\u001a\u00020\bX\u0086D¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n*(\u0010\u0011\u001a\u0004\b\u0000\u0010\r\"\u000e\u0012\u0004\u0012\u0002H\r\u0012\u0004\u0012\u00020\u00030\u00012\u000e\u0012\u0004\u0012\u0002H\r\u0012\u0004\u0012\u00020\u00030\u0001¨\u0006\u0012"},
   d2 = {"num", "Lkotlin/Function1;", "", "", "Ltest/Printable;", "getNum", "()Lkotlin/jvm/functions/Function1;", "value", "", "getValue", "()I", "nonCapLambda", "runLambda2", "T", "x", "a", "(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V", "Printable", "kotlin_bytecode_examples"}
)
public final class NonCapLambdaKt {
   @NotNull
   private static final Function1<Number, Unit> num;
   private static final int value;

   @NotNull
   public static final Function1<Number, Unit> getNum() {
      return num;
   }

   public static final <T> void runLambda2(@NotNull Function1<? super T, Unit> x, T a) {
      Intrinsics.checkNotNullParameter(x, "x");
      x.invoke(a);
   }

   public static final int getValue() {
      return value;
   }

   public static final void nonCapLambda() {
      runLambda2(num, value);
   }

   static {
      num = (Function1)1.INSTANCE;
      value = 5;
   }
}
