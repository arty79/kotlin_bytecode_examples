package test;

import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import test.CapLambdaKt.cap.1;

@Metadata(
   mv = {1, 6, 0},
   k = 2,
   xi = 48,
   d1 = {"\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\n\u001a&\u0010\t\u001a\u00020\u00022\u0016\u0010\u0000\u001a\u0012\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00020\u0001j\u0002`\u00032\u0006\u0010\n\u001a\u00020\u0002\u001a\u0006\u0010\u000b\u001a\u00020\u0002\"!\u0010\u0000\u001a\u0012\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00020\u0001j\u0002`\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0004\u0010\u0005\"\u0014\u0010\u0006\u001a\u00020\u0002X\u0086D¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\b*\"\u0010\f\"\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00020\u00012\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00020\u0001¨\u0006\r"},
   d2 = {"cap", "Lkotlin/Function1;", "", "Ltest/CapSum;", "getCap", "()Lkotlin/jvm/functions/Function1;", "capValue", "getCapValue", "()I", "runLambda3", "a", "сapLambda", "CapSum", "kotlin_bytecode_examples"}
)
public final class CapLambdaKt {
   @NotNull
   private static final Function1<Integer, Integer> cap;
   private static final int capValue;

   @NotNull
   public static final Function1<Integer, Integer> getCap() {
      return cap;
   }

   public static final int getCapValue() {
      return capValue;
   }

   public static final int runLambda3(@NotNull Function1<? super Integer, Integer> cap, int a) {
      Intrinsics.checkNotNullParameter(cap, "cap");
      return ((Number)cap.invoke(a)).intValue();
   }

   public static final int сapLambda() {
      return runLambda3(cap, capValue);
   }

   static {
      cap = (Function1)1.INSTANCE;
      capValue = 5;
   }
}
