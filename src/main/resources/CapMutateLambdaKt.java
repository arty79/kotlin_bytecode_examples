package test;

import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Ref.IntRef;
import org.jetbrains.annotations.NotNull;
import test.CapMutateLambdaKt.mutatingLambda.1;

@Metadata(
   mv = {1, 6, 0},
   k = 2,
   xi = 48,
   d1 = {"\u0000$\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a\u0019\u0010\u0000\u001a\u00020\u00012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0002\u0010\u0005\u001a\u0006\u0010\u0006\u001a\u00020\u0007\u001a\u001f\u0010\b\u001a\u0002H\t\"\u0004\b\u0000\u0010\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\t0\u000b¢\u0006\u0002\u0010\f¨\u0006\r"},
   d2 = {"main", "", "args", "", "", "([Ljava/lang/String;)V", "mutatingLambda", "", "runLambda", "T", "x", "Lkotlin/Function0;", "(Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;", "kotlin_bytecode_examples"}
)
public final class CapMutateLambdaKt {
   public static final <T> T runLambda(@NotNull Function0<? extends T> x) {
      Intrinsics.checkNotNullParameter(x, "x");
      return x.invoke();
   }

   public static final int mutatingLambda() {
      IntRef x = new IntRef();
      runLambda((Function0)(new 1(x)));
      return x.element;
   }

   public static final void main(@NotNull String[] args) {
      Intrinsics.checkNotNullParameter(args, "args");
      System.out.println(mutatingLambda());
   }
}
