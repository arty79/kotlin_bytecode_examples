package examples

const val ZERO2 = 1
const val ONE2 = 1

fun constWhen2(x: Int): String = when(x){
    0 -> "zero"
    1 -> "one"
    else -> "many"
}

