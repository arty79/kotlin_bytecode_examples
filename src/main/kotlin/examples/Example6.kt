package examples

import java.lang.StringBuilder


fun StringBuilder.up(): String {
    return this.append("Kotlin-banzai!").toString()
}

fun main(args: Array<String>) {
    println(StringBuilder("Hello-").up())
}


