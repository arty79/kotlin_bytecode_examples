package examples

import kotlin.jvm.internal.Intrinsics

class E {
    fun x(s: String) {
        println(s)
    }

    private fun y(s: String) {
        println(s)
    }

    fun test(first: String, second: String?) : String {
        second ?: return first
        Intrinsics.throwNpe()
        return "$first $second"
    }

}

