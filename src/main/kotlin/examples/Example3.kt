package examples

data class B(val x: Int, val y: Long)

//Destructuring declarations
fun func() {
    val (x, y) = B(10, 2)
}

