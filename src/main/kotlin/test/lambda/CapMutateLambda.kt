package test

fun <T> runLambda(x: () -> T): T = x()
fun mutatingLambda(): Int {
    var x = 0
    runLambda { x++ }
    return x
}

fun main(args: Array<String>) {
    println(mutatingLambda())
}





