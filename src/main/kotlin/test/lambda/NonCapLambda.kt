package test

typealias Printable<T> = (T) -> Unit
// Определяем очень простое лямбда-выражение, кторое делает
// вывод в консоль значения
val num: Printable<Number> = { a: Number -> println("Hi!") }
fun <T> runLambda2(x: Printable<T>, a: T): Unit = x(a)
val value = 5
fun nonCapLambda(): Unit = runLambda2(num, value)



