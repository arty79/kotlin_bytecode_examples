package test

typealias Sum = (Int, Int) -> Int
// Определяем очень простое лямбда-выражение, суммирующее три числа
val sum: Sum = { a, b -> a + b }

fun runLambda1(x: Sum, a: Int, b: Int): Int = x(a, b)