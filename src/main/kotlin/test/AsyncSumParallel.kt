import java.util.concurrent.TimeUnit
import kotlin.coroutines.Continuation
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext
import kotlin.coroutines.resume
import kotlin.coroutines.startCoroutine
import kotlin.coroutines.suspendCoroutine


suspend fun suspendFunctionWithDelay2(a: Int, b: Int): Int {
    println("${Thread.currentThread()} step 1")
    delay(1000)
    println("${Thread.currentThread()} step 2")
    return a + b
}

suspend fun delay(ms: Long) {
    suspendCoroutine<Unit> { continuation ->
        executor.schedule({ continuation.resume(Unit) }, ms, TimeUnit.MILLISECONDS)
    }
}

fun startAndForget(suspendingFunction: suspend () -> Unit) {
    suspendingFunction.startCoroutine(object : Continuation<Unit> {
        override fun resumeWith(result: Result<Unit>) {
            // forget it
        }
        override val context: CoroutineContext
            get() = EmptyCoroutineContext
    })
}

fun main(args: Array<String>) {
    println("${Thread.currentThread()} main started")
    startAndForget {
        val message = suspendFunctionWithDelay2(40, 2)
        println("${Thread.currentThread()} result is $message")
    }
    executor.shutdown()
    println("${Thread.currentThread()}  main ended")
}

