fun simpleFunctionWithDelaySimple(a: Int, b: Int): Int {
    println("${Thread.currentThread()} step 1")
    Thread.sleep(1000)
    println("${Thread.currentThread()} step 2")
    return a + b
}


fun main(args: Array<String>) {
    println("${Thread.currentThread()} main started")
    val message = simpleFunctionWithDelaySimple(40, 2)
    println("${Thread.currentThread()} result is $message")
    println("${Thread.currentThread()}  main ended")
}

