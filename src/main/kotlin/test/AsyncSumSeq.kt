import java.util.concurrent.CompletableFuture
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import kotlin.coroutines.Continuation
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext
import kotlin.coroutines.resume
import kotlin.coroutines.startCoroutine
import kotlin.coroutines.suspendCoroutine


val executor = Executors.newScheduledThreadPool(1)
suspend fun suspendFunctionWithDelay(a: Int, b: Int): Int {
    println("${Thread.currentThread()} step 1")
    delay(1000)
    println("${Thread.currentThread()} step 2")
    return a + b
}
fun startAndGetFuture(suspendingFunction: suspend () -> Unit): CompletableFuture<Unit> {
    val future = CompletableFuture<Unit>()
    suspendingFunction.startCoroutine(object : Continuation<Unit> {
        override fun resumeWith(result: Result<Unit>) {
            future.complete(result.getOrThrow())
        }
        override val context: CoroutineContext
            get() = EmptyCoroutineContext
    })
    return future
}

fun main(args: Array<String>) {
    println("${Thread.currentThread()} main started")
    val future = startAndGetFuture {
        val message = suspendFunctionWithDelay(40, 2)
        println("${Thread.currentThread()} result is $message")
    }
    future.get()
    executor.shutdown()
    println("${Thread.currentThread()}  main ended")
}

