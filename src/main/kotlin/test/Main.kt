package test

import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


suspend fun main() = coroutineScope {

    val job: Job = async {
        println("Some coroutin")
        delay(400L)
        launch {
            for (i in 1..5) {
                println(i)
                delay(400L)
            }
        }
        println("Start")
        println("End")
    }

}