package test

import kotlinx.coroutines.*

fun main(args: Array<String>) {
    val res = mutableListOf<String>()
    runBlocking<Unit> {
        launch {
            expensiveComputation(res)
        }
        res.add("Hello,")
    }
    println(res.joinToString(" "))
}

suspend fun expensiveComputation(res: MutableList<String>) {
    delay(1000L)
    res.add("world!")
}