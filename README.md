При декомпиляции применялись следущие инструменты

Fernflower — опенсорсный декомпилятор, на текущий момент разрабатываемый и поддерживаемый компанией JetBrains. Код можно посмотреть на GitHub https://github.com/JetBrains/intellij-community/tree/master/plugins/java-decompiler/engine

java -jar fernflower.jar -dgs=1 -asc=1 -ind="    " <input-jar> <output-dir>

java -cp fernflower.jar org.jetbrains.java.decompiler.main.decompiler.ConsoleDecompiler -hdc=0 -dgs=1 -rsy=1 -lit=1 -lac=1 -inn=1 -ind="    " ./src ./target/kotlin_bytecode_examples-1.0.jar ./out

Некоторые параметры запуска

hdc (1): hide empty default constructor
dgs (0): decompile generic signatures
rsy (0): hide synthetic class members
lit (0): output numeric literals "as-is"
inn (1): check for IntelliJ IDEA-specific @NotNull annotation and remove inserted code if found
lac (0): decompile lambda expressions to anonymous classes
vac (1)
log (INFO)

https://github.com/6168218c/FernflowerUI/releases/download/v3.4.2.1/FernFlowerUI3.4.2.1-Static.exe - UI версия fernflower

CFR (0.146) — декомпилятор, написанный, судя по всему, одним человеком, который говорит, что сделал это "for fun". Репозиторий на GitHub. https://github.com/leibnitz27/cfr
Сайт с информацией о проекте http://www.benf.org/other/cfr/index.html
